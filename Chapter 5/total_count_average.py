#TASK 5.5 - EXERCISE 1

#initialise variables
total = 0
count = 0

#prompt for value from user
value = input('Enter a number: ')

while value != 'done' :
    #validate user value
    try:
        fvalue = float(value)
    except:
        print('Bad Data')
        print('Invalid Input')

        #prompt for next value from user
        value = input('Enter a number: ')

        continue

    #calculate count and running total
    count = count + 1
    total = total + fvalue

    #prompt for next value from user
    value = input('Enter a number: ')

#calculate average
average = total / count

#print final total, final count and average
print(total, count, "%.3f" % round(average, 3))
