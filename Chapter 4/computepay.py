#Chapter 4 - Exercise 6

import sys

#Function definition to calculate pay including time-and-a-half for overtime
def computepay(hours, rate):
    if hours > 40:
        pay = (40 * rate) + ((hours - 40) * rate * 1.5)
    else:
        pay = hours * rate
#   print('Pay:', pay)
    print("Pay: %.2f" % round(pay, 2))


#Input hours as string and convert to floating point
str_hours = input('Please enter your hours for the week: ')
try:
    fhours = float(str_hours)
except:
    print('Error, please enter numeric input')
    sys.exit()


#Input rate as string and convert to floating point
str_rate = input('Please enter your hourly rate: ')
try:
    frate = float(str_rate)
except:
    print('Error, please enter numeric input')
    sys.exit()


#Function call to calculate pay using floating point values
computepay(fhours, frate)

