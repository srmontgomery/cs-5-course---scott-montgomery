#Chapter 4 - Exercise 7

import sys

#Define Function to return grade when score is input
def computegrade(score):
    if score < 0.0 :
        return 'Bad score'
    elif score > 1.0 :
        return 'Bad score'
    elif score >= 0.9 :
        return 'A'
    elif score >= 0.8 :
        return 'B'
    elif score >= 0.7 :
        return 'C'
    elif score >= 0.6 :
        return 'D'
    elif score < 0.6 :
        return 'F'


#Accept score as a string
str_score = input('Enter score: ')


#Convert score from string to float
try:
    fscore = float(str_score)
except:
    print('Bad score')
    sys.exit()


#Determine grade and print it to the screen
grade = computegrade(fscore)
print(grade)

