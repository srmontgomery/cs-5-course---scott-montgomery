import sys


hours = input('Please enter your hours for the week: ')
try:
    fhours = float(hours)
except:
    print('Error, please enter numeric input')
    sys.exit()


rate = input('Please enter your hourly rate: ')
try:
    frate = float(rate)
except:
    print('Error, please enter numeric input')
    sys.exit()


if fhours > 40:
    pay = (40 * frate) + ((fhours - 40) * frate * 1.5)
else:
    pay = fhours * frate


print('Pay:', pay)
